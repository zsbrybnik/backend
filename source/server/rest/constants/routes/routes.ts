enum Routes {
  Login = "/login",
  User = "/user",
  UserWithId = "/user/:id",
  PostWithId = "/post/:id",
  Post = "/post",
  VerifyToken = "/verify-token",
  GenerateRandomPassword = "/generate-random-password",
  Posts = "/posts",
  Page = "/page",
  PageWithName = "/page/:name",
  Weather = "/weather",
}

export default Routes;
