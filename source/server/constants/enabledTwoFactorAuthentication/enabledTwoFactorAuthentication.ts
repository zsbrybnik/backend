enum EnabledTwoFactorAuthentication {
  Disabled = "disabled",
  Application = "application",
  Phone = "phone",
  PhoneWithCalls = "phoneWithCalls",
}

export default EnabledTwoFactorAuthentication;
