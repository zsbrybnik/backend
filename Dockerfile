FROM node:latest AS build
ADD . /home/backend
WORKDIR /home/backend
RUN apt-get -qq update 
RUN apt-get install -y -q build-essential curl golang-go 
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN yarn --ignore-engines
RUN npx prisma generate --schema=./source/server/prisma/schema.prisma
RUN yarn test
RUN yarn run build
EXPOSE 3000
CMD ["yarn", "run", "run"]
